Draw.loadPlugin(function(ui) {
    // Créez un nouveau bouton dans la barre d'outils
    ui.toolbar.addSeparator();
    var helloButton = ui.toolbar.addButton('helloButton', function() {
        alert('Bonjour');
    });

    // Définissez l'icône du bouton
    helloButton.firstChild.style.backgroundImage = 'url(chemin_vers_votre_icône)'; // Remplacez 'chemin_vers_votre_icône' par l'URL de votre icône
    helloButton.firstChild.style.backgroundRepeat = 'no-repeat';
    helloButton.firstChild.style.backgroundPosition = '2px 3px';

    // Événement de nettoyage lors de la fermeture de l'éditeur
    ui.editor.addListener('destroy', function(sender, evt) {
        // Supprimez le bouton lorsque l'éditeur est fermé
        helloButton.parentNode.removeChild(helloButton);
    });
});
